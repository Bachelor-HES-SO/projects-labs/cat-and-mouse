Fe=8000;
r=load('test_cat.txt');
len=length(r);
x=(0:len-1);

for i=852:853
  f = i;
  ReFf0 = 0;
  ImFf0 = 0;
  for l=1:len
    ReFf0 += r(l) * cos(-2 * pi * (f / Fe) * l);
    ImFf0 += r(l) * sin(-2 * pi * (f / Fe) * l);
  end
  f
  ReFf0
  ImFf0
  somme = abs(ReFf0) / 2000000 + abs(ImFf0) / 2000000
  if abs(ReFf0) / 2000000 + abs(ImFf0) / 2000000 > 1
    f
    ReFf0
    ImFf0
    somme = abs(ReFf0) / 2000000 + abs(ImFf0) / 2000000
  end
end
