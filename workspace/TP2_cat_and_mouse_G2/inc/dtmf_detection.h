#ifndef DTMF_DETECTION_H
#define DTMF_DETECTION_H

#include "objects.h"

void init_dtmf();
dir_t read_dtmf(int buf_idx);

#endif
