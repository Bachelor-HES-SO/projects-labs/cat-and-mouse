/*
 * game.h
 *
 *  Created on: 17 mai 2017
 *      Author: Orphee
 */

#ifndef GAME_H_
#define GAME_H_

#ifndef VAR_DECLS
#define _DECL extern
#define _INIT(x)
#else
#define _DECL
#define _INIT(x) = x
#endif

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

// FreeRTOS libraries
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

// MyLab_lib libraries
#include "lcd.h"
#include "debug.h"
#include "uart.h"
#include "trace_mgt.h"
#include "objects.h"
#include "adc.h"
#include "dtmf_detection.h"
#include "ethmac.h"
#include "custom_rand.h"

// myLab2_lib libraries
#include "gpio.h"
#include "utils.h"
#include "timer.h"

typedef struct {
	object_t cat, mouse;
	sprites_t sprites;
	uint8_t mouse_vitality;
	uint8_t mouse_score;
	uint8_t cat_score;
} game_t;

typedef enum {
	FREQ1 = 852,
	FREQ2 = 941,
	FREQ3 = 1336,
	FREQ4 = 1608
} freq_t;

#define SLEEP(mseconds)	vTaskDelay(mseconds / portTICK_RATE_MS)
#define SLEEP_UNTIL(prev_wake_time, mseconds) vTaskDelayUntil(prev_wake_time, mseconds / portTICK_RATE_MS)
#define TASK_CREATE(function, name, args, priority) xTaskCreate(function, (signed portCHAR*)name, configMINIMAL_STACK_SIZE, args, priority, NULL)

#define FONT_COLOR			LCD_BLACK
#define BACKGROUND_COLOR 	LCD_WHITE
#define MAX_SCORE			3
#define SCORE_X				6
#define MOUSE_SCORE_Y		300
#define CAT_SCORE_Y			4

#define MOUSE_X				34
#define MOUSE_Y				280
#define MOUSE_MAX_VITALITY	75
#define MOUSE_BOTTOM_POS	278

#define CAT_DEBUG			0
#define CAT_X				110
#define CAT_Y				30
#define CAT_SPEED			4
#define CAT_BOTTOM_POS		250

#define BUF_SIZE 	400
#define FE 			8000
#define PI 			3.14159265359
#define AMPLITUDE	10000
#define MODULE		((400 * AMPLITUDE) / 2)

#define portMIN_DELAY	(portTickType) 0
#define MOUSE_SPEED(v)	(((3 * v + (MOUSE_MAX_VITALITY / 2)) / MOUSE_MAX_VITALITY) + 1)
#define FILLED_GAUGE()	lcd_filled_rectangle(220, 300, 232, 318, LCD_GREEN);
#define GAME_OVER()		game->mouse_score == MAX_SCORE || game->cat_score == MAX_SCORE

__DATA(RAM1) _DECL xSemaphoreHandle stop_display;
__DATA(RAM1) _DECL xSemaphoreHandle score_sem;
__DATA(RAM1) _DECL xSemaphoreHandle mouse_restart;
__DATA(RAM1) _DECL xSemaphoreHandle cat_restart;

__DATA(RAM1) _DECL xQueueHandle mouse_queue;
__DATA(RAM1) _DECL xQueueHandle vitality_queue;
__DATA(RAM1) _DECL xQueueHandle cat_queue;
__DATA(RAM1) _DECL xQueueHandle data_queue;

_DECL portTickType display_last_wake_time;
_DECL portTickType mouse_last_wake_time;
_DECL portTickType cat_last_wake_time;

_DECL uint16_t *sig;       // pointer on acquisition double buffer
__DATA(RAM1) _DECL float cos_tab[4][BUF_SIZE];
__DATA(RAM1) _DECL float sin_tab[4][BUF_SIZE];

void compute_cos_sin(void);
void cat_move(void *arg);
void mouse_move(void *arg);
void display(void *arg);

#endif /* GAME_H_ */
