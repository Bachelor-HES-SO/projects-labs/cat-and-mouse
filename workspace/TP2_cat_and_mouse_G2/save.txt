uint8_t compute_module(uint8_t buf_index) {
	uint16_t i;
	double reF1 = 0, imF1 = 0;
	double reF2 = 0, imF2 = 0;
	double reF3 = 0, imF3 = 0;
	double reF4 = 0, imF4 = 0;
	bool is_freq1, is_freq2, is_freq3, is_freq4;
	for (i = buf_index * BUF_SIZE; i < BUF_SIZE + BUF_SIZE * buf_index; i++) {
		reF1 += (double)sig[i] * cos_tab[0][i];
		imF1 += (double)sig[i] * sin_tab[0][i];
		reF2 += (double)sig[i] * cos_tab[1][i];
		imF2 += (double)sig[i] * sin_tab[1][i];
		reF3 += (double)sig[i] * cos_tab[2][i];
		imF3 += (double)sig[i] * sin_tab[2][i];
		reF4 += (double)sig[i] * cos_tab[3][i];
		imF4 += (double)sig[i] * sin_tab[3][i];
	}
	is_freq1 = ((fabs(reF1) / MODULE + fabs(imF1) / MODULE) > 1);
	is_freq2 = ((fabs(reF2) / MODULE + fabs(imF2) / MODULE) > 1);
	is_freq3 = ((fabs(reF3) / MODULE + fabs(imF3) / MODULE) > 1);
	is_freq4 = ((fabs(reF4) / MODULE + fabs(imF4) / MODULE) > 1);
	if (is_freq1 && is_freq3) return JOYSTICK_UP;
	if (is_freq2 && is_freq3) return JOYSTICK_RIGHT;
	if (is_freq1 && is_freq4) return JOYSTICK_DOWN;
	if (is_freq2 && is_freq4) return JOYSTICK_LEFT;
	return 0;
}

uint8_t check_cat_joystick(uint8_t buf_index) {
	//int2file("test_cat.txt", &sig[buf_index * BUF_SIZE], BUF_SIZE, sizeof(uint16_t), false);
	return compute_module(buf_index);
}