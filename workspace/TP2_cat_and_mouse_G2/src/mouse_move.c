/**
 * @file		mouse_move.c
 * @brief		mouse_move task implementation
 *
 * @author		Raed Abdennadher
 * @author		Orphée Antoniadis
 * @author		Steven Liatti
 * @bug			No known bugs.
 * @date		May 21, 2017
 */

#include <game.h>

/**
 * @brief 		Init the mouse state (direction, position, vitality)
 *
 * @param 		game	all the informations on the game
 */
void init_mouse(game_t *game) {
	game->mouse.dir = NORTH;
	game->mouse.pos.x = (rnd_32() % 3) * 80 + MOUSE_X;
	game->mouse.pos.y = MOUSE_Y;
	game->mouse_vitality = MOUSE_MAX_VITALITY;
}

/**
 * @brief 		Callback function called each time the joystick is pressed or no
 * 				after the game is finished. It checks only if the center of the
 * 				joystick is pressed, if yes it restarts the game by giving
 * 				semaphores.
 *
 * @param 		pos 	position of the joystick (0 if not pressed)
 * @param		arg		NULL pointer
 */
void mouse_reset_callback(uint8_t pos, void *arg) {
	if (pos == JOYSTICK_CENTER) {
		xSemaphoreGive(mouse_restart);
		xSemaphoreGive(stop_display);
		xSemaphoreGive(cat_restart);
	}
}

/**
 * @brief 		Callback function called each time the joystick is pressed or no
 * 				before the end of the game. It updates the mouse's position
 * 				and vitality.
 *
 * @param 		pos 	position of the joystick (0 if not pressed)
 * @param		arg		pointer on the game struct
 */
void mouse_move_callback(uint8_t pos, void *arg) {
	game_t *game = (game_t*)arg;
	switch(pos) {
		case JOYSTICK_UP:
			if (game->mouse.pos.y != TOP_POS) {
				game->mouse.dir = NORTH;
				if (game->mouse.pos.y - MOUSE_SPEED(game->mouse_vitality) < TOP_POS) game->mouse.pos.y = TOP_POS;
				else game->mouse.pos.y -= MOUSE_SPEED(game->mouse_vitality);
				if (game->mouse_vitality > 0) game->mouse_vitality--;
			}
			else if (game->mouse_vitality < MOUSE_MAX_VITALITY) game->mouse_vitality++;
			break;
		case JOYSTICK_LEFT:
			if (game->mouse.pos.x != 0) {
				game->mouse.dir = WEST;
				if ((int)game->mouse.pos.x - MOUSE_SPEED(game->mouse_vitality) < 0) {
					game->mouse.pos.x = 0;
				}
				else game->mouse.pos.x -= MOUSE_SPEED(game->mouse_vitality);
				if (game->mouse_vitality > 0) game->mouse_vitality--;
			}
			else if (game->mouse_vitality < MOUSE_MAX_VITALITY) game->mouse_vitality++;
			break;
		case JOYSTICK_DOWN:
			if (game->mouse.pos.y != MOUSE_BOTTOM_POS) {
				game->mouse.dir = SOUTH;
				if (game->mouse.pos.y + MOUSE_SPEED(game->mouse_vitality) > MOUSE_BOTTOM_POS) game->mouse.pos.y = MOUSE_BOTTOM_POS;
				else game->mouse.pos.y += MOUSE_SPEED(game->mouse_vitality);
				if (game->mouse_vitality > 0) game->mouse_vitality--;
			}
			else if (game->mouse_vitality < MOUSE_MAX_VITALITY) game->mouse_vitality++;
			break;
		case JOYSTICK_RIGHT:;
			uint16_t temp = LCD_MAX_WIDTH - game->sprites.mouse_im[EAST].width - 1 ;
			if (game->mouse.pos.x != temp) {
				game->mouse.dir = EAST;
				if (game->mouse.pos.x + MOUSE_SPEED(game->mouse_vitality) > temp) game->mouse.pos.x = temp;
				else game->mouse.pos.x += MOUSE_SPEED(game->mouse_vitality);
				if (game->mouse_vitality > 0) game->mouse_vitality--;
			}
			else if (game->mouse_vitality < MOUSE_MAX_VITALITY) game->mouse_vitality++;
			break;
		default :
			if (game->mouse_vitality < MOUSE_MAX_VITALITY) game->mouse_vitality++;
			break;
	}
}

/**
 * @brief 		Task that updates the position and the vitality of the mouse
 * 				by looking every 20ms the position of the joystick.
 *
 * @param 		arg		pointer on the sprites struct
 */
void mouse_move(void *arg) {
	mouse_last_wake_time = xTaskGetTickCount();
	game_t game;
	sprites_t *sprites = (sprites_t*)arg;
	init_mouse(&game);
	game.sprites = *sprites;
	while(1) {
		if (xSemaphoreTake(mouse_restart, portMIN_DELAY) != pdTRUE) joystick_handler(mouse_reset_callback, NULL, POLLING);
		else {
			if (xSemaphoreTake(score_sem, portMIN_DELAY) == pdTRUE) {
				init_mouse(&game);
				xQueueSend(mouse_queue, (void *)&(game.mouse), portMIN_DELAY);
				xQueueSend(vitality_queue, (void *)&(game.mouse_vitality), portMIN_DELAY);
				xSemaphoreGive(stop_display);
			}
			else {
				joystick_handler(mouse_move_callback, &game, POLLING);
				xQueueSend(mouse_queue, (void *)&(game.mouse), portMIN_DELAY);
				xQueueSend(vitality_queue, (void *)&(game.mouse_vitality), portMIN_DELAY);
			}
			xSemaphoreGive(mouse_restart);
		}
		SLEEP_UNTIL(&mouse_last_wake_time, 20);
	}
}
