/**
 * @file		cat_move.c
 * @brief		cat_move task implementation
 *
 * @author		Raed Abdennadher
 * @author		Orphée Antoniadis
 * @author		Steven Liatti
 * @bug			No known bugs.
 * @date		May 21, 2017
 */

#include <game.h>

/**
 * @brief 		Init the cat state (direction, position)
 *
 * @param 		game	all the informations on the game
 */
void init_cat(game_t *game) {
	game->cat.dir = SOUTH;
	game->cat.pos.x = CAT_X;
	game->cat.pos.y = CAT_Y;
}

/**
 * @brief 		Vertical to horizontal rotation detection.
 *
 * @param 		old	last state of the direction
 * @param		new	new state of the direction
 *
 * @return true if a V to H rotation is detect, 0 otherwise
 */
bool detect_VtoH_rotation(dir_t old, dir_t new) {
	return (old == NORTH && new == WEST) || (old == NORTH && new == EAST) ||
			(old == SOUTH && new == WEST) || (old == SOUTH && new == EAST);
}

/**
 * @brief 		Horizontal to vertical rotation detection.
 *
 * @param 		old	last state of the direction
 * @param		new	new state of the direction
 *
 * @return true if a H to V rotation is detect, 0 otherwise
 */
bool detect_HtoV_rotation(dir_t old, dir_t new) {
	return (old == WEST && new == NORTH) || (old == WEST && new == SOUTH) ||
			(old == EAST && new == NORTH) || (old == EAST && new == SOUTH);
}

/**
 * @brief 		Function called after analysis of the signal send by the other
 * 				card. It updates the postion of the cat by looking witch position
 * 				of the joystick of the other card has been pressed.
 * 				If CAT_DEBUG the function is a callback function on the joystick
 * 				of the current card.
 *
 * @param 		pos 	position of the joystick (0 if not pressed)
 * @param		arg		pointer on the game struct
 */
void cat_move_callback(uint8_t pos, void *arg) {
	game_t *game = (game_t*)arg;
	object_t cat_last_state = game->cat;
	switch(pos) {
		case JOYSTICK_UP:
			game->cat.dir = NORTH;
			if (game->cat.pos.y != TOP_POS) {
				if (game->cat.pos.y - CAT_SPEED < TOP_POS) game->cat.pos.y = TOP_POS;
				else game->cat.pos.y -= CAT_SPEED;
			}
			break;
		case JOYSTICK_LEFT:
			game->cat.dir = WEST;
			if (game->cat.pos.x != 0) {
				if (game->cat.pos.x - CAT_SPEED < 0) game->cat.pos.x = 0;
				else game->cat.pos.x -= CAT_SPEED;
			}
			break;
		case JOYSTICK_DOWN:
			game->cat.dir = SOUTH;
			if (game->cat.pos.y != CAT_BOTTOM_POS) {
				if (game->cat.pos.y + CAT_SPEED > CAT_BOTTOM_POS) game->cat.pos.y = CAT_BOTTOM_POS;
				else game->cat.pos.y += CAT_SPEED;
			}
			break;
		case JOYSTICK_RIGHT:;
			uint16_t temp = LCD_MAX_WIDTH - game->sprites.cat_im[EAST].width - 1 ;
			game->cat.dir = EAST;
			if (game->cat.pos.x != temp) {
				if (game->cat.pos.x + CAT_SPEED > temp) game->cat.pos.x = temp;
				else game->cat.pos.x += CAT_SPEED;
			}
			break;
	}
	if (detect_VtoH_rotation(cat_last_state.dir, game->cat.dir)) {
		game->cat.pos.x = game->cat.pos.x - (game->sprites.cat_im[NORTH].width / 2 + 1);
		game->cat.pos.y = game->cat.pos.y + (game->sprites.cat_im[EAST].height / 2 + 1);
		if (game->cat.pos.x < 0) game->cat.pos.x = 0;
		uint16_t temp = LCD_MAX_WIDTH - game->sprites.cat_im[EAST].width - 1 ;
		if (game->cat.pos.x > temp) game->cat.pos.x = temp;
	}
	if (detect_HtoV_rotation(cat_last_state.dir, game->cat.dir)) {
		game->cat.pos.x = game->cat.pos.x + (game->sprites.cat_im[NORTH].width / 2 + 1);
		game->cat.pos.y = game->cat.pos.y - (game->sprites.cat_im[EAST].height / 2 + 1);
		if (game->cat.pos.y < TOP_POS) game->cat.pos.y = TOP_POS;
		if (game->cat.pos.y + CAT_SPEED > CAT_BOTTOM_POS) game->cat.pos.y = CAT_BOTTOM_POS;
	}
}

/**
 * @brief 		Function used to compute the cosine and the sinus used
 * 				in module formula once. We made it to avoid multiples
 * 				calls of cos() and sin().
 */
void compute_cos_sin(void) {
	uint16_t i;
	for (i = 0; i < BUF_SIZE; i++) {
		cos_tab[0][i] = cos(-2 * PI * ((float)FREQ1 / FE) * i);
		sin_tab[0][i] = sin(-2 * PI * ((float)FREQ1 / FE) * i);
		cos_tab[1][i] = cos(-2 * PI * ((float)FREQ2 / FE) * i);
		sin_tab[1][i] = sin(-2 * PI * ((float)FREQ2 / FE) * i);
		cos_tab[2][i] = cos(-2 * PI * ((float)FREQ3 / FE) * i);
		sin_tab[2][i] = sin(-2 * PI * ((float)FREQ3 / FE) * i);
		cos_tab[3][i] = cos(-2 * PI * ((float)FREQ4 / FE) * i);
		sin_tab[3][i] = sin(-2 * PI * ((float)FREQ4 / FE) * i);
	}
}

/**
 * @brief 		Function used to calculate the module of the signal send
 * 				for 1 of the 4 frequencies. It calculates absolute values of
 * 				real and imaginary part of signal. After that looks if the
 * 				value is greater than 1.
 *
 * @param 		buf_index	Index of the buffer to look at
 * @param		freq_id		Index of the freq to use
 *
 * @return		1 if the signal has a peak on the freq tested, 0 otherwise
 */
bool compute_module(uint8_t buf_index, uint16_t freq_id) {
	uint16_t i;
	float reF = 0;
	float imF = 0;
	for (i = buf_index * BUF_SIZE; i < BUF_SIZE + BUF_SIZE * buf_index; i++) {
		reF += (float)sig[i] * cos_tab[freq_id][i % BUF_SIZE];
		imF += (float)sig[i] * sin_tab[freq_id][i % BUF_SIZE];
	}
	return (fabs(reF) / MODULE + fabs(imF) / MODULE) > 1;
}

/**
 * @brief 		Checks the position of the other card joystick by calculating
 * 				module and looking if the signal has a peak in 1 of the 4 frequencies.
 * 				It then return the good position of joystick by a series of conditions.
 *
 * @param 		buf_index	Index of the buffer to look at
 *
 * @return		position of joystick, 0 if not pressed
 */
uint8_t check_cat_joystick(uint8_t buf_index) {
	//int2file("test_cat.txt", &sig[buf_index * BUF_SIZE], BUF_SIZE, sizeof(uint16_t), false);
	if (compute_module(buf_index, 0)) {
		if (compute_module(buf_index, 2)) return JOYSTICK_UP;
		else if (compute_module(buf_index, 3)) return JOYSTICK_DOWN;
		return 0;
	}
	else if (compute_module(buf_index, 1)) {
		if (compute_module(buf_index, 2)) return JOYSTICK_RIGHT;
		else if (compute_module(buf_index, 3)) return JOYSTICK_LEFT;
		return 0;
	}
	return 0;
}

/**
 * @brief 		Task that updates the position of the cat by checking every
 * 				50ms what the adc sends to the double buffer describe
 * 				in dtmf_detection.c. In CAT_DEBUG, the joystick of the current
 * 				card is directly checked.
 *
 * @param 		arg		pointer on the sprites struct
 */
void cat_move(void *arg) {
	cat_last_wake_time = xTaskGetTickCount();
	game_t game;
	sprites_t *sprites = (sprites_t*)arg;
	init_cat(&game);
	game.sprites = *sprites;
	uint8_t buf_index;
	uint8_t pos;
	while(1) {
		if (xSemaphoreTake(cat_restart, portMIN_DELAY) != pdTRUE) {
			init_cat(&game);
			if (cat_queue != 0) xQueueSend(cat_queue, (void *)&(game.cat), portMIN_DELAY);
			xSemaphoreTake(cat_restart, portMAX_DELAY);
			xSemaphoreGive(cat_restart);
		} else {
			xSemaphoreGive(cat_restart);
			if (CAT_DEBUG) joystick_handler(cat_move_callback, &game, POLLING);
			else {
				if(xQueueReceive(data_queue, &buf_index, portMIN_DELAY)) {
					pos = check_cat_joystick(buf_index);
					cat_move_callback(pos, &game);
				}
			}
			xQueueSend(cat_queue, (void *)&(game.cat), portMIN_DELAY);
			SLEEP_UNTIL(&cat_last_wake_time, 50);
		}
	}
}
