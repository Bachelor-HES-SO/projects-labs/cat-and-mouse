/**
 * @file		display.c
 * @brief		display task implementation
 *
 * @author		Raed Abdennadher
 * @author		Orphée Antoniadis
 * @author		Steven Liatti
 * @bug			No known bugs.
 * @date		May 21, 2017
 */

#include <game.h>

/**
 * @brief 		Init the game main display (cheese, mouse pits, gauge, ...)
 *
 * @param 		sprites   Array that contains all the sprites of the game
 */
void init_display(sprites_t sprites) {
	uint8_t i;
	for (i = 0; i < 3; i++) {
		display_bitmap16(sprites.cheese_im.bitmap, 28+80*i, 0, sprites.cheese_im.width, sprites.cheese_im.height);
		lcd_filled_rectangle(30+80*i, 313, 30+12+80*i, 319, LCD_BLACK);		// mouse pits
		lcd_filled_circle(30+6+80*i, 313, 6, LCD_BLACK);
	}
	lcd_empty_rectangle(219, 299, 233, 319, LCD_BLACK);						// empty gauge
	FILLED_GAUGE();
}

/**
 * @brief 		Detects collisions between 2 rectangular objects. As soon as the
 * 				distance between both objects if lower than or equal to the margin,
 * 				the collision is detected.
 *
 * @param 		obj1	positions and directions of the first object
 * @param		obj2	positions and directions of the second object
 * @param		obj1_im	reference to first object images (containing the dimensions)
 * @param		obj2_im	reference to second object images (containing the dimensions)
 * @param		margin	distance of collision detection between the 2 objects
 *
 * @return		true if the collision is detected, false otherwise
 */
bool detect_collision(object_t obj1, image_t *obj1_im, object_t obj2, image_t *obj2_im, int margin) {
	return ((obj1.pos.x-obj2.pos.x<(int)obj2_im[obj2.dir].width+margin-1) &&
		(obj2.pos.x-obj1.pos.x<(int)obj1_im[obj1.dir].width+margin-1) &&
		(obj2.pos.y-obj1.pos.y<(int)obj1_im[obj1.dir].height+margin-1) &&
		(obj1.pos.y-obj2.pos.y<(int)obj2_im[obj2.dir].height+margin-1));
}

/**
 * @brief 		Compare 2 positions and return if they are equal
 *
 * @param 		a   first position
 * @param		b	second position
 *
 * @return		0 if not equal 1 otherwise
 */
uint8_t pos_cmp(pos_t a, pos_t b) {
	if (a.x != b.x) return 0;
	if (a.y != b.y) return 0;
	return 1;
}

/**
 * @brief 		Display an object on the screen
 *
 * @param 		object	positions and directions of the object
 * @param		sprite	reference to object images (containing the dimensions)
 */
void display_object(object_t object, image_t *sprite) {
	uint16_t x = object.pos.x;
	uint16_t y = object.pos.y;
	uint16_t width = sprite[object.dir].width;
	uint16_t height = sprite[object.dir].height;
	display_bitmap16(sprite[object.dir].bitmap, x, y, width, height);
}

/**
 * @brief 		Clear just a part of the object to make the display smoother.
 *
 * @param 		object	positions and directions of the object
 * @param		sprite	reference to object images (containing the dimensions)
 * @param		speed of the object to refresh
 */
void refresh_object(object_t object, image_t *sprite, uint8_t speed) {
	uint16_t x = object.pos.x;
	uint16_t y = object.pos.y;
	uint16_t width = sprite[object.dir].width;
	uint16_t height = sprite[object.dir].height;
	switch(object.dir) {
		case NORTH:
			lcd_filled_rectangle(x, y + height - speed, x + width, y + height, BACKGROUND_COLOR);
			break;
		case SOUTH:
			lcd_filled_rectangle(x, y, x + width, y + speed, BACKGROUND_COLOR);
			break;
		case WEST:
			lcd_filled_rectangle(x + width - speed, y, x + width, y + height, BACKGROUND_COLOR);
			break;
		case EAST:
			lcd_filled_rectangle(x, y, x + speed, y + height, BACKGROUND_COLOR);
			break;
	}
}

/**
 * @brief 		Clear an object on the screen
 *
 * @param 		object	positions and directions of the object
 * @param		sprite	reference to object images (containing the dimensions)
 */
void clear_object(object_t object, image_t *sprite) {
	uint16_t x = object.pos.x;
	uint16_t y = object.pos.y;
	uint16_t width = sprite[object.dir].width;
	uint16_t height = sprite[object.dir].height;
	lcd_filled_rectangle(x, y, x + width, y + height, BACKGROUND_COLOR);
}

/**
 * @brief 		Increase the level of the gauge
 *
 * @param 		last	last state of the gauge
 * @param		new		new state of the gauge
 */
void increase_gauge(uint16_t last, uint16_t new) {
	lcd_filled_rectangle(220, 318 - new, 232, 318 - last, LCD_GREEN);
}

/**
 * @brief 		Decrease the level of the gauge
 *
 * @param 		last	last state of the gauge
 * @param		new		new state of the gauge
 */
void decrease_gauge(uint16_t last, uint16_t new) {
	lcd_filled_rectangle(220, 318 - new, 232, 318 - last, BACKGROUND_COLOR);
}

/**
 * @brief 		Checks if the mouse touches a cheese and if it does, update the
 * 				score, the position and the vitality of the mouse.
 *
 * @param 		game	all the informations on the game
 */
void check_mouse_win(game_t *game) {
	if (game->mouse.pos.y == TOP_POS) {
		xSemaphoreGive(score_sem);
		clear_object(game->mouse, game->sprites.mouse_im);
		game->mouse_score++;
		lcd_print(SCORE_X, MOUSE_SCORE_Y, BIGFONT, FONT_COLOR, BACKGROUND_COLOR, "%d", game->mouse_score);
		FILLED_GAUGE();
		xSemaphoreTake(stop_display, portMAX_DELAY);
	}
}

/**
 * @brief 		Checks if the cat touches a mouse and if it does, update the
 * 				the position and the vitality of the mouse and the score of
 * 				the cat.
 *
 * @param 		game	all the informations on the game
 */
void check_cat_win(game_t *game) {
	if (detect_collision(game->cat, game->sprites.cat_im, game->mouse, game->sprites.mouse_im, 1)) {
		xSemaphoreGive(score_sem);
		clear_object(game->mouse, game->sprites.mouse_im);
		game->cat_score++;
		display_object(game->cat, game->sprites.cat_im);
		lcd_print(SCORE_X, CAT_SCORE_Y, BIGFONT, FONT_COLOR, BACKGROUND_COLOR, "%d", game->cat_score);
		FILLED_GAUGE();
		xSemaphoreTake(stop_display, portMAX_DELAY);
	}
}

/**
 * @brief 		Checks if the game is over by looking the score and if it does,
 * 				clear all the game and wait on a semaphore the joystick to be
 * 				pressed. When pressed, it resets the game and the display.
 *
 * @param 		game	all the informations on the game
 */
uint8_t check_end(game_t *game) {
	if (GAME_OVER()) {
		clear_object(game->mouse, game->sprites.mouse_im);
		clear_object(game->cat, game->sprites.cat_im);
		lcd_print(65, 160, SMALLFONT, FONT_COLOR, BACKGROUND_COLOR, "Press joystick");
		xSemaphoreTake(mouse_restart, portMAX_DELAY);
		xSemaphoreTake(cat_restart, portMAX_DELAY);
		xSemaphoreTake(stop_display, portMAX_DELAY);
		lcd_print(65, 160, SMALLFONT, BACKGROUND_COLOR, BACKGROUND_COLOR, "Press joystick");
		return 1;
	} else {
		SLEEP_UNTIL(&display_last_wake_time, 16);
		return 0;
	}
}

/**
 * @brief 		Display all the objects of the game by looking into the
 * 				game struct that contains all the informations of the
 * 				game (mouse and cat coords, vitality level, sprites, ...)
 *
 * @param 		arg		pointer on the sprites struct
 */
void display(void *arg) {
	display_last_wake_time = xTaskGetTickCount();
	game_t game;
	sprites_t *sprites = (sprites_t*)arg;
	game.sprites = *sprites;
	init_display(game.sprites);
	while(1) {
		object_t mouse_last_state = {{120,160}, NORTH};
		object_t cat_last_state = {{120,160}, NORTH};
		uint8_t last_vitality_state = 0;
		uint16_t gauge_state, gauge_last_state;
		game.mouse_score = 0;
		game.cat_score = 0;
		lcd_print(SCORE_X, MOUSE_SCORE_Y, BIGFONT, FONT_COLOR, BACKGROUND_COLOR, "%d", game.mouse_score);
		lcd_print(SCORE_X, CAT_SCORE_Y, BIGFONT, FONT_COLOR, BACKGROUND_COLOR, "%d", game.cat_score);
		do {
			gauge_state = game.mouse_vitality / (MOUSE_MAX_VITALITY / 18);
			gauge_last_state = last_vitality_state / (MOUSE_MAX_VITALITY / 18);
			if(xQueueReceive(vitality_queue, &(game.mouse_vitality), portMIN_DELAY)) {
				if (last_vitality_state < game.mouse_vitality) increase_gauge(gauge_last_state, gauge_state);
				else if (last_vitality_state > game.mouse_vitality) decrease_gauge(gauge_last_state, gauge_state);
			}
			if(xQueueReceive(mouse_queue, &(game.mouse), portMIN_DELAY)) {
				if (pos_cmp(game.mouse.pos, mouse_last_state.pos) == 0) {
					if (mouse_last_state.dir != game.mouse.dir)
						clear_object(mouse_last_state, game.sprites.mouse_im);
					else
						refresh_object(mouse_last_state, game.sprites.mouse_im, MOUSE_SPEED(last_vitality_state));
					display_object(game.mouse, game.sprites.mouse_im);
					check_mouse_win(&game);
				}
			}
			if(xQueueReceive(cat_queue, &(game.cat), portMIN_DELAY)) {
				if (pos_cmp(game.cat.pos, cat_last_state.pos) == 0) {
					if (cat_last_state.dir != game.cat.dir)
						clear_object(cat_last_state, game.sprites.cat_im);
					else
						refresh_object(cat_last_state, game.sprites.cat_im, CAT_SPEED);
					display_object(game.cat, game.sprites.cat_im);
				}
			}
			check_cat_win(&game);
			mouse_last_state = game.mouse;
			cat_last_state = game.cat;
			last_vitality_state = game.mouse_vitality;
		} while (!check_end(&game));
	}
}
