/**
 * @file		tp2_cat_and_mouse.c
 * @brief		cat and mouse game with 2 mylab2 boards. FreeRTOS
 * 				and LCD display used.
 *
 * @author		Raed Abdennadher
 * @author		Orphée Antoniadis
 * @author		Steven Liatti
 * @bug			No known bugs.
 * @date		May 21, 2017
 * @version		0.6.1
 */

#define VAR_DECLS
#include <game.h>

int main(void) {
	uart0_init(115000);
	static sprites_t sprites;
	int i;

	ethernet_power_down();				// remove potential noise on ADC due to Ethernet module
	init_lcd();
	init_rnd32(42);
	clear_screen(BACKGROUND_COLOR);

	// load all necessary sprites
	sprites.cat_im[0].bitmap=read_bmp_file("cat 24x48 up.bmp", &sprites.cat_im[0].width, &sprites.cat_im[0].height);
	sprites.cat_im[1].bitmap=read_bmp_file("cat 24x48 right.bmp", &sprites.cat_im[1].width, &sprites.cat_im[1].height);
	sprites.cat_im[2].bitmap=read_bmp_file("cat 24x48 down.bmp", &sprites.cat_im[2].width, &sprites.cat_im[2].height);
	sprites.cat_im[3].bitmap=read_bmp_file("cat 24x48 left.bmp", &sprites.cat_im[3].width, &sprites.cat_im[3].height);
	sprites.mouse_im[0].bitmap=read_bmp_file("mouse 12x20 up.bmp", &sprites.mouse_im[0].width, &sprites.mouse_im[0].height);
	sprites.mouse_im[1].bitmap=read_bmp_file("mouse 12x20 right.bmp", &sprites.mouse_im[1].width, &sprites.mouse_im[1].height);
	sprites.mouse_im[2].bitmap=read_bmp_file("mouse 12x20 down.bmp", &sprites.mouse_im[2].width, &sprites.mouse_im[2].height);
	sprites.mouse_im[3].bitmap=read_bmp_file("mouse 12x20 left.bmp", &sprites.mouse_im[3].width, &sprites.mouse_im[3].height);

	if ((sprites.cheese_im.bitmap=read_bmp_file("cheese 32x26.bmp", &sprites.cheese_im.width, &sprites.cheese_im.height))==NULL)
		EXIT("Not enough space to create image!");

	for (i=0; i<4; i++)
		if (sprites.cat_im[i].bitmap==NULL || sprites.mouse_im[i].bitmap==NULL)
			EXIT("Not enough space to create image!");

	init_dtmf();

	if ((stop_display = xSemaphoreCreateCounting(1,0)) == NULL)
		EXIT("Cannot create stop_display semaphore");
	if ((score_sem = xSemaphoreCreateCounting(1,0)) == NULL)
		EXIT("Cannot create score_sem semaphore");
	if ((mouse_restart = xSemaphoreCreateCounting(1,1)) == NULL)
		EXIT("Cannot create mouse_restart semaphore");
	if ((cat_restart = xSemaphoreCreateCounting(1,1)) == NULL)
		EXIT("Cannot create cat_restart semaphore");

	if ((mouse_queue = xQueueCreate(10, sizeof(object_t))) == 0)
		EXIT("Cannot create mouse queue");
	if ((vitality_queue = xQueueCreate(10, sizeof(uint8_t))) == 0)
		EXIT("Cannot create vitality queue");
	if ((cat_queue = xQueueCreate(10, sizeof(object_t))) == 0)
		EXIT("Cannot create cat queue");
	if ((data_queue = xQueueCreate(1, sizeof(uint8_t))) == 0)
		EXIT("Cannot create data queue");

	compute_cos_sin();

	if (TASK_CREATE(mouse_move, "Mouse task", &sprites, tskIDLE_PRIORITY + 1) != pdPASS)
		EXIT("Cannot create mouse task");
	if (TASK_CREATE(display, "Display task", &sprites, tskIDLE_PRIORITY + 1) != pdPASS)
		EXIT("Cannot create display task");
	if (TASK_CREATE(cat_move, "Cat task", &sprites, tskIDLE_PRIORITY + 1) != pdPASS)
		EXIT("Cannot create cat task");

	stopTimer(TIMER0);
	startTimer(TIMER0);
	vTaskStartScheduler();

	while(1);
	return 1;
}
