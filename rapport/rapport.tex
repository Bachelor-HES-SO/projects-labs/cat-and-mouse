\documentclass[a4paper]{article}
\title{TP2 : Cat and mouse \protect\\ Programmation temps-réel}
\author{Groupe 2 \\ Raed Abdennadher \hspace{0.5cm} Orphée Antoniadis \hspace{0.5cm} Steven Liatti}
\usepackage[francais]{babel}
\usepackage{fontspec}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{enumitem}
\usepackage{minted}
\usemintedstyle{colorful}
\setlength{\parindent}{0pt}
\usepackage[left=2cm,top=2cm,right=2cm,bottom=2cm]{geometry}
\usepackage{caption}

\begin{document}
\maketitle

\section{Introduction}
Le but de notre travail était de réaliser le jeu du chat et de la souris. Le principe
est simple, un joueur contrôle un chat, l'autre une souris, la souris doit essayer
d'atteindre les fromages sans que le chat ne l'attrape. Le premier joueur ayant
réussi 3 fois son objectif gagne. Le jeu se joue avec 2 cartes MyLab 2 communiquant
par signaux audio décriptés par un adc. Pour ce travail, nous n'avons eu qu'à implémenter
le récepteur des signaux, l'émetteur nous étant donné.

\section{État général du développement}
Les spécifications demandées dans l'énoncé sont entièrement fonctionnelles et expliquées
plus bas. Nous n'avons remarqué aucun bug au moment du rendu. Le bonus d'implémentation
de l'émetteur n'a par contre pas été fait.

\subsection{Structure du programme}
\begin{center}
\includegraphics[scale=0.6]{images/cat_and_mouse.png}
\captionof{figure}{Schéma bloc de notre programme}
\end{center}
\newpage

\subsection{Description des tâches}
\subsubsection{Déplacement du chat}
Le déplacement du chat se fait en deux parties. Il y a d'abord tout le travail déjà fourni
de récupération des signaux dans un double buffer et ensuite l'analyse de ces signaux
en utiliant la transformée de Fourier. Afin de faire fonctionner la fonction de callback
\mintinline{c}{buffer_filled} avec notre tâche \mintinline{c}{cat_move}
nous avons eu recourt à une queue (\mintinline{c}{data_queue}). Lorsque la fonction de callback
est appelée, la seule chose que nous faisons est d'envoyer l'index du buffer qui a été
rempli (pour rappel, la fonction de callback est appelée chaque fois que l'un des
deux buffers est rempli). Cette queue ne contient donc que des 0 et des 1. Les données
envoyées dans cette queue sont donc reçues dans \mintinline{c}{cat_move} puis
données à la fonction \mintinline{c}{check_cat_joystick}. Cette fonction va appliquer
une méthode de calcul du module de la transformée de Fourier aux valeurs converties
par l'adc. \medbreak

Nous savons que le module de la transformée de Fourier est décrit comme
$|F(f_x)| = |\sum_{i=0}^{n} y(i)e^{-j 2 \pi \frac{f_x}{F_e} i}|$. \medbreak
Nous savons aussi qu'un signal sinusoïdal complexe s'écrit
$e^{j 2 \pi f t} = cos(2 \pi f t) + jsin(2 \pi f t)$. \medbreak
Appliquons maintenant ces formules à notre situation :

\begin{align}
  |F(f_x)| &= |\sum_{i=0}^{399} y(i)e^{-j 2 \pi \frac{f_x}{F_e} i}| \\
  |F(f_x)| &= |\sum_{i=0}^{399} y(i)cos(-2 \pi \frac{f_x}{F_e} i) + jsin(2 \pi \frac{f_x}{F_e} i)| \\
  |F(f_x)| &= |Re[\sum_{i=0}^{399} y(i)e^{-j 2 \pi \frac{f_x}{F_e} i}]| + |Im[\sum_{i=0}^{399} y(i)e^{-j 2 \pi \frac{f_x}{F_e} i}]| \\
  |F(f_x)| &= |\sum_{i=0}^{399} y(i)cos(-2 \pi \frac{f_x}{F_e} i)| + |\sum_{i=0}^{399} y(i)sin(-2 \pi \frac{f_x}{F_e} i)|
\end{align}

Nous avons maintenant une équation exploitable dans notre programme (sans nombres complexes).
Il nous a suffit de l'utiliser avec nos données donc avec le bon $f_x$ et le bon jeu de données
et comparer le résultat au seuil décrit dans l'énoncé ($\frac{400A}{2}$) pour savoir
si le signal reçu contient un pic à la fréquence donnée. \medbreak

Une fois les signaux analysés et la position du joystick décryptée, le chat va changer
de position et de direction ou ne rien changer puis envoyer ses nouvelles coordonnées
dans une autre queue, \mintinline{c}{cat_queue}. La dernière primitive de synchronisation
utile à cette tâche est un sémaphore (\mintinline{c}{cat_restart}) qui permet de signaler
si un des joueurs a gagné, de bloquer la tâche jusqu'à l'appui du bouton central
du joystick et enfin de réinitialiser la position du chat.

\subsubsection{Déplacement de la souris}
Le déplacement de la souris est beaucoup moins compliqué que celui du chat. Il reprend
des mécaniques déjà utilisées dans les travaux précédents. Ici, la tâche \mintinline{c}{mouse_move}
va vérifier l'état du joystick toutes les 20ms et va changer ou non la position de la souris.
Toute cette mécanique se fait à partir d'une fonction de callback passée à une fonction
plus bas niveau qui vérifie l'état des pins GPIO du joystick. Nous utilisons, comme
dans \mintinline{c}{cat_move}, une queue (\mintinline{c}{mouse_queue}) pour envoyer
la nouvelle position de la souris à la tâche d'affichage et de gestion du jeu mais
nous avons eu aussi recourt à une deuxième (\mintinline{c}{vitality_queue}) afin
d'envoyer la vitalité de la souris qui doit être gérée ici. \medbreak

Le gain de point d'un des deux joueurs est signalé par le sémaphore \mintinline{c}{score_sem}
et la  fin du jeu est signalée par le sémaphore \mintinline{c}{mouse_restart}.
Contrairement à la tâche du chat, la tâche de la souris ne doit pas s'arrêter lors
de la fin du jeu. En effet, étant donné que c'est ici que l'état du joystick est
vérifié, c'est cette tâche qui va relancer les autres lors de l'appui sur le bouton
central du joystick. Lorsque cette action est effectuée, les sémaphores correspondant
aux tâches bloquées sont rendus et le jeu reprend.

\newpage

\subsubsection{Affichage et gestion du jeu}
Comme demandé dans l'énoncé, cette tâche va récupérer l'état du chat et de la souris
dans les queues décrites plus haut puis va les afficher seulement si leur position
a changé depuis le dernier affichage. Afin de rendre l'affichage plus fluide, nous avons
fait une fonction (\mintinline{c}{refresh_object}) qui va seulement effacer la bande
de la précédente image qui est en dehors de la nouvelle. Une fois avoir mis à jour
les positions des objets et la jauge, cette tâche va détecter si un des joueurs a
marqué un point à l'aide des fonction \mintinline{c}{check_mouse_win} et
\mintinline{c}{check_cat_win}. Si un des joueurs a bien marqué, la souris
est notifiée à l'aide des mécanismes expliqués plus haut puis la tâche va bloquer
sur le sémaphore \mintinline{c}{stop_display}. Elle sera libérée lorsque la souris
aura bien réinitialisé sa position. \medbreak

Toutes ces tâches sont effectuées en boucle tant qu'aucun joueur n'a marqué 3 points.
Une fois ceci fait la tâche le notifie (de nouveau comme expliqué précédement) et
s'arrête une seconde fois sur le sémaphore \mintinline{c}{stop_display}. Cette fois-ci,
il faudra que le joueur de la souris appuit sur le bouton central du joystick pour la
libérer. L'affichage est ensuite réinitialisé puis le jeu recommence.

\subsubsection{Évacuation des traces}
L'évacuation des traces se fait de la même manière que pour les précédents travaux.
Nous utilisons une tâche de basse priorité (\mintinline{c}{vApplicationIdleHook})
ainsi qu'un buffer circulaire qui contient les traces. Ce buffer est un simple
tableau de \mintinline{c}{trace_t} avec des curseurs pour savoir où nous en sommes
dans le tableau. Chaque fois que la tâche de basse priorité est appelée, le buffer
est vidé et ses données envoyées à l'UART.

\section{Analyse des traces}
\begin{center}
\includegraphics[scale=0.45]{images/traces.png}
\captionof{figure}{Extrait des traces des tâches}
\end{center}
Dans les traces que nous avons recueilli, sig000 est la tâche \mintinline{c}{mouse_move},
sig001 est la tâche \mintinline{c}{display} et sig002 est la tâche \mintinline{c}{cat_move}.
Nous pouvons d'ailleurs les reconnaitre sans savoir dans quel ordre nous les avons
initialisées car leurs timings sont précis et visibles sur ces traces. En effet, toutes
les préemptions n'ont aucun retard ce qui nous permet aussi de constater qu'il
n'y a pas de gigue.




\end{document}
